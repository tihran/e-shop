<?php

class Product extends Controller
{

    public function __construct()
    {
        parent::__construct();
    }

    public function view($id)
    {
        $this->view->id = $id;
        $this->view->productSingleList = $this->model->productSingleList($id);
        $this->view->render('product/index');
    }
    public function buy($id) {
        $this->model->buy($id);
    }

}