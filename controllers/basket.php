<?php

class Basket extends Controller {

    public function __construct(){
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        $role = Session::get('role');
        if($logged == false || $role == 'ADMIN') {
            header('location: ../index');
            exit;
        }
    }

    public function view(){
        $this->view->title = "Basket";
        $this->view->basketList = $this->model->basketList();
        $this->view->render('basket/index');
    }

    public function confirm(){
        $email = $_POST['email'];
        $text = "Hello, ".$_POST['name']."!<br>";
        $text .= "Your order is accepted<br>";
        if(mail($email, "d", $text)) {
            header('location: ../index');
        }
        else {
            echo "error";
        }
    }

    public function delete($id){
        $this->model->delete($id);
    }

}