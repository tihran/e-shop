<?php

class Admin extends Controller {

    public function __construct(){
        parent::__construct();
        Session::init();
        $logged = Session::get('loggedIn');
        $role = Session::get('role');
        if($logged == false || $role != 'ADMIN'){
            header('location: ../index');
            exit;
        }
    }

    public function index(){
        $this->view->title = "Admin";
        $this->view->productList = $this->model->productList();
        $this->view->categoryList = $this->model->categoryList();
        $this->view->render('admin/index');
    }

    public function createCategory(){
        $this->model->createCategory();
    }

    public function createProduct(){
        $this->model->createProduct();
    }

    public function editProduct($id){
        $this->view->title = "Edit Product";
        $this->view->product = $this->model->productSingleList($id);
        $this->view->categoryList = $this->model->categoryList();
        $this->view->render('admin/editProduct');
    }

    public function editCategory($id){
        $this->view->title = "Edit Category";
        $this->view->category = $this->model->categorySingleList($id);
        $this->view->render('admin/editCategory');
    }

    public function saveProduct($id){
        $this->model->saveProduct($id);
    }

    public function saveCategory($id){
        $this->model->saveCategory($id);
    }

    public function delete($table, $id){
        $this->model->delete($table, $id);
    }
}