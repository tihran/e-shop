<?php


class Category extends Controller{

    public function __construct(){
        parent::__construct();
    }

    public function view($title){
        $this->view->title = $title;
        $this->view->category = $this->model->category($title);
        $this->view->render('category/index');
    }

}