<?php

class Index extends Controller {

    public function __construct(){
        parent::__construct();
        Session::init();
    }

    public function index(){
        $this->view->title = "Main Page";
        $this->view->categoryList = $this->model->categoryList();
        $this->view->render('index/index');
    }

    public function logout(){
        Session::destroy();
        header('location: ../login');
        exit;
    }

}