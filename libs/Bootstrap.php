<?php

class Bootstrap
{
    private $_url = null;
    private $load = "";

    private $_controller = null;

    private $_controllerPath = 'controllers/';
    private $_modelPath = 'models/';
    private $_errorFile = 'error.php';

    public function init()
    {
        $this->getUrl();
        if (empty($this->_url[0])) {
            return header('location: ../index');
        }
        $this->_loadExistingController();
        $this->_callControllerMethod();
    }

    private function getUrl(){
        $url = isset($_GET['url']) ? $_GET['url'] : null;
        $url = rtrim($url, '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $this->_url = explode('/', $url);
    }

    private function _loadExistingController(){
        $this->load = $this->_url[0];

        $file = $this->_controllerPath . $this->load . '.php';

        if (file_exists($file)) {
            require $file;
            $this->_controller = new $this->load;
            $this->_controller->loadModel($this->load, $this->_modelPath);
        }
        else {
            $this->_error();
            exit;
        }
    }

    private function _callControllerMethod(){

        $length = count($this->_url);

        if ($length > 1) {
            $method = $this->_url[1];
            if (!method_exists($this->_controller, $method)) {
                $this->_error();
                exit;
            }
        }

        switch ($length) {
            case 5:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3], $this->_url[4]);
                break;
            case 4:
                $this->_controller->{$this->_url[1]}($this->_url[2], $this->_url[3]);
                break;
            case 3:
                $this->_controller->{$this->_url[1]}($this->_url[2]);
                break;
            case 2:
                $this->_controller->{$this->_url[1]}();
                break;
            default:
                $this->_controller->index();
                break;
        }
    }

    private function _error() {
        require $this->_controllerPath . $this->_errorFile;
        $this->_controller = new Error();
        $this->_controller->index();
        return false;
    }

}
