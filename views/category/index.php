<div class="container">
    <h1 align="center"><b><?=(isset($this->title)) ? $this->title : 'MVC'; ?></b></h1>
    <hr>
    <br>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th>Description</th>
            <th>Price (UAH)</th>
            <th>Quantity</th>
        </tr>
        </thead>
        <?php

        foreach($this->category as $key=>$value){
            echo '<tr>';
            echo '<td>'.$value['id'].'</td>';
            echo '<td><a href="'.URL.'product/view/'.$value['id'].'">'.$value['title'].'</td>';
            echo '<td>'.$value['description'].'</td>';
            echo '<td>'.$value['price'].'</td>';
            echo '<td>'.$value['quantity'].'</td>';
            echo '</tr>';
        }
        ?>
    </table>
</div>