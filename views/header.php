<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title><?=(isset($this->title)) ? $this->title : 'MVC'; ?></title>
    <link rel="stylesheet" type="text/css" href="<?php echo URL; ?>public/css/default.css">
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap.min.css" rel="stylesheet">
    <link href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.2/css/select2.min.css" rel="stylesheet" />
</head>
<body>
<?php Session::init();?>


<nav class="navbar navbar-inverse">
    <div class="container-fluid">
        <div class="navbar-header">
            <a class="navbar-brand" href="<?php echo URL; ?>index">Home</a>
            <?php if(Session::get('role') == 'ADMIN'): ?>
            <a class="navbar-brand" href="<?php echo URL; ?>admin">Admin</a>
            <?php endif; ?>
        </div>
        <ul class="nav navbar-nav navbar-right">
            <?php if(Session::get('loggedIn') == false): ?>
            <li> <a href="<?php echo URL; ?>login"><span class="glyphicon glyphicon-log-in"></span> Login</a></li>
            <?php else: ?>
                <?php if(Session::get('role') != 'ADMIN'): ?>
                    <li><a href="<?php echo URL; ?>basket/view/<?php echo Session::get('user_id')?>"><span class="glyphicon glyphicon-shopping-cart"></span>Basket</a></li>
                <?php endif; ?>
                <li><a href="<?php echo URL; ?>index/logout"><span class="glyphicon glyphicon-log-out"></span>Logout</a></li>
            <?php endif; ?>
        </ul>
    </div>
</nav>

</body>
</html>
