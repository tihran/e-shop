<div class="container">
    <h1 align="center">Admin page</h1>
    <hr />
    <div class="col-sm-6">
        <h3><b>Create a Category</b></h3>

        <form role="form" method="post" action="<?php echo URL; ?>admin/createCategory">
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" required>
            </div>
            <br/>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
    <div class="col-sm-6">
        <h3><b>Create a Product</b></h3>

        <form role="form" method="post" action="<?php echo URL; ?>admin/createProduct">
            <div class="form-group">
                <label for="title">Title:</label>
                <input type="text" name="title" class="form-control" required>
            </div>
            <div class="form-group">
                <label for="description">Description:</label>
                <input type="text" class="form-control" name="description" required>
            </div>
            <div class="form-group">
                <label for="price">Price</label>
                <input type="text" class="form-control" name="price" required>
            </div>
            <div class="form-group">
                <label for="quantity">Quantity</label>
                <input type="text" class="form-control" name="quantity" required>
            </div>
            <label>Category</label>
            <select name="category" class="form-control" required>
                <?php foreach($this->categoryList as $value): ?>
                <option value="<?php echo $value['title']?>"><?php echo $value['title']?></option>
                <?php endforeach; ?>
            </select>
            <br/>
            <button type="submit" class="btn btn-default">Submit</button>
        </form>
    </div>
</div>

<hr />
<div class="container">
    <h3><b>Current Products</b></h3>
        <table class="table table-hover">
            <thead>
            <tr>
                <th>Id</th>
                <th>Title</th>
                <th>Description</th>
                <th>Quantity</th>
                <th>Price</th>
                <th>Category</th>
                <th colspan="2">Modifications</th>
            </tr>
            </thead>
            <?php
                foreach($this->productList as $key=>$value){

                    echo '<tr>';
                    echo '<td>'.$value['id'].'</td>';
                    echo '<td>'.$value['title'].'</td>';
                    echo '<td>'.$value['description'].'</td>';
                    echo '<td>'.$value['quantity'].'</td>';
                    echo '<td>'.$value['price'].'</td>';
                    echo '<td>'.$value['category'].'</td>';
                    echo '<td><a href="'.URL.'admin/editProduct/'.$value['id'].'">Edit</a></td>
                          <td><a href="'.URL.'admin/delete/products/'.$value['id'].'">Delete</a></td>';
                    echo '</tr>';

                }
            ?>
        </table>
</div><div class="container">
    <h3><b>Current Categories</b></h3>
    <table class="table table-hover">
        <thead>
        <tr>
            <th>Id</th>
            <th>Title</th>
            <th colspan="2">Modifications</th>
        </tr>
        </thead>
        <?php
        foreach($this->categoryList as $key=>$value){

            echo '<tr>';
            echo '<td>'.$value['id'].'</td>';
            echo '<td>'.$value['title'].'</td>';
            echo '<td><a href="'.URL.'admin/editCategory/'.$value['id'].'">Edit</a></td>
                          <td><a href="'.URL.'admin/delete/categories/'.$value['id'].'">Delete</a></td>';
            echo '</tr>';

        }
        ?>
    </table>
</div>
