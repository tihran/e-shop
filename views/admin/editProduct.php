<div class="container">
    <h1 align="center">Notes</h1>
    <hr />
    <h3><b>Edit Product</b></h3>
    <form role="form" method="post" action="<?php echo URL ;?>admin/saveProduct/<?php echo $this->product[0]['id'];?>">
        <div class="form-group">
            <label for="title">Title:</label>
            <input type="text" name="title" class="form-control" value="<?php echo $this->product[0]['title'];?>">
        </div>
        <div class="form-group">
            <label for="description">Description:</label>
            <input type="text" name="description" class="form-control" value="<?php echo $this->product[0]['description'];?>">
        </div>
        <div class="form-group">
            <label for="price">Price:</label>
            <input type="text" name="price" class="form-control" value="<?php echo $this->product[0]['price'];?>">
        </div>
        <div class="form-group">
            <label for="quantity">Quantity:</label>
            <input type="text" name="quantity" class="form-control" value="<?php echo $this->product[0]['quantity'];?>">
        </div>
        <label>Category</label>
        <select name="category" class="form-control">
            <?php foreach($this->categoryList as $value):
                     if($this->product[0]['category'] == $value['title']):?>
                         <option selected="selected" value="<?php echo $value['title']?>"><?php echo $value['title']?></option>
                     <?php else:?>
                         <option value="<?php echo $value['title']?>"><?php echo $value['title']?></option>
            <?php endif; endforeach; ?>
        </select>
        <br/>
        <button type="submit" class="btn btn-default">Submit</button>
    </form>



</div>