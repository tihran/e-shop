<div class="container">

    <div class="col-sm-6">

    <h1><?php echo $this->productSingleList[0]['title'];?></h1>
    <h3><u>Description:</u> <?php echo $this->productSingleList[0]['description']?></h3>
    <h3><u>Price, UAH:</u> <?php echo $this->productSingleList[0]['price']?></h3>
    </div>
    <div class="col-sm-6">
        <form method="post" action="../buy/<?php echo $this->id;?>">
            <?php if(Session::get('role') != 'ADMIN' and Session::get('loggedIn')): ?>
            <button type="submit" class="btn btn-default">Add to basket</button>
                <?php elseif(Session::get('loggedIn') == false): ?>
                <h3>To buy this product you have to be logged in<h3>
            <?php endif?>
        </form>
    </div>
</div>