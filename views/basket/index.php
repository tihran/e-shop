<div class="container">
<table class="table table-hover">
    <thead>
    <tr>
        <th>Product name</th>
        <th>Price</th>
        <th>Delete</th>
    </tr>
    </thead>
    <?php
    $sum = 0;
    foreach($this->basketList as $key=>$value){
        echo '<tr>';
        echo '<td>'.$value['title'].'</td>';
        echo '<td>'.$value['price'].'</td>';
        echo '<td><a href="../delete/'.$value['id'].'">x</a></td>';
        echo '</tr>';
        $sum += $value['price'];
    }
    ?>
    <tr>
        <td colspan="3"><h3><b>Total: </b></h3><?php echo $sum ?> UAH</td>
    </tr>
</table>

    <form role="form" method="post" action="../confirm">
        Name: <input type="text" name="name">
        Phone number: <input type="text" name="phone">
        E-mail: <input type="email" name="email">
        <button class="btn btn-default" type="submit">Confirm</button>
    </form>
</div>
