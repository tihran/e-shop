<?php

spl_autoload_register(function ($class) {
    include 'libs/' .$class.'.php';
});

require 'config.php';

$bootstrap = new Bootstrap();

$bootstrap->init();