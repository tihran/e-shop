<?php

class Admin_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function productList(){
        return $this->db->select('SELECT products.id,products.title,description,quantity,price, categories.title as category
        FROM products JOIN categories on products.category_id = categories.id;');
    }

    public function productSingleList($id){
        return $this->db->select('SELECT products.id,products.title,description,quantity,price, categories.title as category
        FROM products JOIN categories on products.category_id = categories.id
        WHERE products.id=:id;',array('id'=>$id));
    }

    public function categoryList(){
        return $this->db->select('SELECT * FROM categories;');
    }

    public function categorySingleList($id){
        return $this->db->select('SELECT * FROM categories WHERE id=:id;',array('id'=>$id));
    }

    public function createCategory(){
        $this->db->insert('categories', array(
            'title' => $_POST['title'],
        ));
        header('location: '.URL.'admin');
    }

    public function createProduct(){
        $category_id = $this->db->select('SELECT id FROM categories WHERE title=:title',
            array('title'=>$_POST['category']));

        $this->db->insert('products', array(
            'title' => $_POST['title'],
            'description' => $_POST['description'],
            'price' => $_POST['price'],
            'quantity' => $_POST['quantity'],
            'category_id' => $category_id[0]['id']
        ));
        header('location: '.URL.'admin');
    }

    public function saveProduct($id){
        $category_id = $this->db->select('SELECT id FROM categories WHERE title=:title',
            array('title'=>$_POST['category']));

        $this->db->update('products', array(
            'title' => $_POST['title'],
            'description' => $_POST['title'],
            'price' => $_POST['price'],
            'quantity' => $_POST['quantity'],
            'category_id' => $category_id[0]['id']
        ),"`id` = {$id}");
        header('location: '.URL.'admin');
    }

    public function saveCategory($id){
        $this->db->update('categories', array(
            'title' => $_POST['title'],
        ),"`id` = {$id}");
        header('location: '.URL.'admin');
    }

    public function delete($table, $id){
        $this->db->delete($table,"id = $id");
        header('location: '.URL.'admin');
    }

}