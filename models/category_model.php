<?php

class Category_Model extends Model{
    public function __construct()
    {
        parent::__construct();
    }
    public function category($title) {
        $sth = $this->db->prepare("SELECT products.id, products.title, description, quantity, price from products
                JOIN categories ON products.category_id = categories.id
                WHERE categories.title = :title;");
        $sth->execute(array(
            ":title"=>$title));
        return $sth->fetchAll();
    }

}