<?php

class Basket_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function basketList(){
        $sth = $this->db->prepare('
            SELECT products.title, products.price, basket.id from products
            JOIN basket on products.id = basket.product_id
            JOIN users ON basket.user_id = users.id
            WHERE user_id=:user_id;');

        $sth->execute(array(
            ':user_id' => Session::get('user_id'),
        ));
        return $sth->fetchAll();
    }

    public function delete($id){
            $user_id = Session::get('user_id');
            $this->db->delete('basket',"id = $id");
            header('location: '.URL.'basket/view/'.$user_id.'');
    }

}