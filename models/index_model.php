<?php

class Index_Model extends Model{

    public function __construct(){
        parent::__construct();
    }

    public function categoryList(){
        return $this->db->select('SELECT title FROM categories');
    }

}