<?php

class Product_Model extends Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function productSingleList($id)
    {
        $sth = $this->db->prepare('SELECT title,description,price
                       FROM products WHERE id = :id');
        $sth->execute(array(
            ':id' => $id,
        ));
        return $sth->fetchAll();
    }

    public function buy($id)
    {
        $this->db->insert('basket', array(
            'user_id' => Session::get('user_id'),
            'product_id' => $id,
        ));
        header('location: ' . URL . 'basket/view/'.$id.'');
    }
}